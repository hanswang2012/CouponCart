<?php

class CouponCartTest extends PHPUNIT_Framework_TestCase
{
    // dummy test
    public function testEmpty() {
        // create
        $rules = array(
                    'discount_on_multiple' => array(
                        'apply_product' => '9780201835953',
                        'count_threshold' => 10,
                        'discount_price' => 21.99,
                    ),
                    'three_for_price_of_two' => array(
                        'apply_product' => '9781430219484',
                    ),
                    'free_gift_product' => array(
                        'apply_product' => '9325336130810',
                        'gift_product' => '9325336028278',
                    ),
                );
        $cart = new CouponCart($rules);

        //calculate
        $sum = $cart->total();

        // Assert
        $this->assertEquals($sum, 0);
    }

    // test on money discount over coupon 1
    public function testMoneyCounting1()
    {
        // create
        $rules = array(
                    'discount_on_multiple' => array(
                        'apply_product' => '9780201835953',
                        'count_threshold' => 10,
                        'discount_price' => 21.99,
                    ),
                    'three_for_price_of_two' => array(
                        'apply_product' => '9781430219484',
                    ),
                    'free_gift_product' => array(
                        'apply_product' => '9325336130810',
                        'gift_product' => '9325336028278',
                    ),
                );
        $cart = new CouponCart($rules);
        $cart->addProduct("9780201835953");
        $cart->addProduct("9780201835953");
        $cart->addProduct("9780201835953");
        $cart->addProduct("9780201835953");
        $cart->addProduct("9780201835953");
        $cart->addProduct("9780201835953");
        $cart->addProduct("9780201835953");
        $cart->addProduct("9780201835953");
        $cart->addProduct("9780201835953");
        $cart->addProduct("9780201835953");
        $cart->addProduct("9325336028278");

        //calculate
        $sum = $cart->total();

        // Assert
        $this->assertEquals($sum, 239.89);
    }

    // test on money discount over coupon 2
    public function testMoneyCounting2()
    {
        // create
        $rules = array(
                    'discount_on_multiple' => array(
                        'apply_product' => '9780201835953',
                        'count_threshold' => 10,
                        'discount_price' => 21.99,
                    ),
                    'three_for_price_of_two' => array(
                        'apply_product' => '9781430219484',
                    ),
                    'free_gift_product' => array(
                        'apply_product' => '9325336130810',
                        'gift_product' => '9325336028278',
                    ),
                );
        $cart = new CouponCart($rules);
        $cart->addProduct("9781430219484");
        $cart->addProduct("9781430219484");
        $cart->addProduct("9781430219484");
        $cart->addProduct("9780132071482");

        //calculate
        $sum = $cart->total();

        // Assert
        $this->assertEquals($sum, 177.36);
    }

    // test on money discount over coupon 3
    public function testMoneyCounting3()
    {
        // create
        $rules = array(
                    'discount_on_multiple' => array(
                        'apply_product' => '9780201835953',
                        'count_threshold' => 10,
                        'discount_price' => 21.99,
                    ),
                    'three_for_price_of_two' => array(
                        'apply_product' => '9781430219484',
                    ),
                    'free_gift_product' => array(
                        'apply_product' => '9325336130810',
                        'gift_product' => '9325336028278',
                    ),
                );
        $cart = new CouponCart($rules);
        $cart->addProduct("9325336130810");
        $cart->addProduct("9325336028278");
        $cart->addProduct("9780201835953");

        //calculate
        $sum = $cart->total();

        // Assert
        $this->assertEquals($sum, 71.36);
    }

}
