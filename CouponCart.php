<?php

/**
 * CouponCart class implementation
 */
class CouponCart
{
    private $_filters;

    private $_products;

    /**
     * construct from rules
     */
    public function __construct($rules)
    {
        $this->_products = array();
        $this->_filters  = array();
        foreach ($rules as $couponType => $couponSetting) {
            $filter                  = '_' . str_replace('_', '', lcfirst(ucwords($couponType, '_')));
            $this->_filters[$filter] = $couponSetting;
        }
    }

    /**
     * Add new product
     */
    public function addProduct($id)
    {
        if (!isset($this->_products[$id])) {
            $this->_products[$id] = array('count' => 1, 'unit_price' => $this->_getPriceById($id));
        } else {
            $this->_products[$id]['count'] += 1;
        }
    }

    /**
     * Calculate total mondy
     */
    public function total()
    {
    	$priceDiff = $priceSum = 0;
        foreach ($this->_products as $productId => $productDetail) {
    		$priceSum += $productDetail['unit_price'] * $productDetail['count'];
    	}
    	foreach ($this->_filters as $filter => $setting) {
    		if (method_exists($this, $filter)) {
    			$priceDiff += $this->$filter($setting);
    		}
    	}
        return $priceSum - $priceDiff;
    }

    /**
     * first filter
     */
    private function _discountOnMultiple($setting)
    {
        $priceDiff = 0;
        foreach ($this->_products as $productId => $productDetail) {
            if ($productId == $setting['apply_product'] && $productDetail['count'] >= $setting['count_threshold']) {
                $priceDiff = ($productDetail['unit_price'] - $setting['discount_price']) * $productDetail['count'];

                break;
            }
        }

        return $priceDiff;
    }

    /**
     * second filter
     */
    private function _threeForPriceOfTwo($setting)
    {
        $priceDiff = 0;
        foreach ($this->_products as $productId => $productDetail) {
            if ($productId == $setting['apply_product'] && $productDetail['count'] >= 3) {
                $priceDiff = $productDetail['unit_price'] * floor($productDetail['count'] / 3);

                break;
            }
        }

        return $priceDiff;
    }

    /**
     * third filter
     */
    private function _freeGiftProduct($setting)
    {
        $priceDiff = 0;
        $giftProductCount = $purchaseProductCount = 0;
        foreach ($this->_products as $productId => $productDetail) {
            if ($productId == $setting['apply_product']) {
                $purchaseProductCount += 1;
            }
            if ($productId == $setting['gift_product']) {
            	$giftProductCount += 1;
            }
        }
        if ($giftProductCount > 0 && $purchaseProductCount > 0) {
        	$priceDiff = $this->_products[$setting['gift_product']]['unit_price'] * min(array($giftProductCount, $purchaseProductCount));
        }

        return $priceDiff;
    }

    private function _getPriceById($id) {
    	switch ($id) {
    		case '9325336130810':
    			return 39.49;
    		case '9325336028278':
    			return 19.99;
    		case '9780201835953':
    			return 31.87;
    		case '9781430219484':
    			return 28.72;
    		case '9780132071482':
    			return 119.92;
    		default:
    			return 0;
    	}
    }
}
