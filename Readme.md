#CouponCart class which allows for discounts to be applied to a customers cart

* use any language you wish
* do not use any external libraries (Zend etc.)
* do not use a database
* do not create a GUI (we are only interested in your implementation)

Yay, just simple class and test, try not to overkill.

Script to run unittest
```sh
phpunit --bootstrap CouponCart.php CouponCartTest.php
```
